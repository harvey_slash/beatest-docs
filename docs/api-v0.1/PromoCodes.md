# Promo Code APIs

For marketing purposes, items can be purchased at discounted rates.
To allow for this, we support PromoCodes. 

A promo code is supposed to be hidden , that is why all promo codes should not be 
listable openly.

A particular PromoCode can only be used once by any particular user.

Using a PromoCode during checkout   will reduce the [Order](Orders.md) amount 
by the value associated with the promocode.

## Database Table
Promo Codes are currently represented in the database like below

| Field Name           | Data Type                                | Description                                       |
| -------------------- | ---------------------------------------- | ------------------------------------------------- |
| id                   | `int (PK)`                               | Unique identifier for Promo Code                  |
| promo_code           | `varchar(50)`                            | Promo Code Name                                   |
| promo_value          | `varchar(50)`                            | Monetary Value of Promo Code                      |
| promo_used           | `int`                                    | Count of Promo Code usage                         |
| promo_max_usage      | `int`                                    | Maximum Usage Count allowed                       |
| promo_valid          | `tinyint(1)`                             | If Promo Code is Valid                            |
| promo_multiple_use   | `tinyint(1)`                             | Allows multiple usage of Promo Code by a User     |
    

## API Reference

### Validate Promo Code :white_check_mark:
Checks validity of Promo Code by checking if it is active, hasn't already been used by the User and hasn't exceeded the Max usage.

!!! get_req "`POST /promo_codes/:promoId/validate`"
    ```promoID: string```: the Id of the promo code to Update.

	##### Response [1]
    If promo code can be used, an `HTTP 200` response with 
    the value of the promo code will be returned.
	```JSON
	{
		"promo_value": 20,    # monetary value for the promo code
	}
    ```

	##### Response [2]
    If promo code cannot be used, an `HTTP 400` (Error Object) will be returned
	```JSON
	{
        "message": "Invalid Promo Code"
	}
	```

### Admin --> Get Promo Code Details :white_check_mark:
Get details of a particular promo code  by ID

!!! get_req "`GET /promo_codes/:promoId`"
	```promoID: string```: the Id of the promo code to get details for.
	This id is a globally unique identifier for that promo code. 
	
	##### Response
	```JSON
	{
		"promo_code": "BTT012",       # the id that was queried for
		"promo_value": 20,            # monetary value for that promo code
		"promo_used": 12,             # count of promo code usage
        "promo_max_usage": 50,        # maximum count of promo code usage
        "promo_valid": True,          # whether promo code is active
        "promo_multiple_usage": False # whether promo code can be used more than once by a user
	}
	```


### Admin --> Create PromoCode :white_check_mark:
Register a promo code offer by giving the details.

!!! post_req "`POST /admin/promo_codes`"
	##### Request
	```JSON
	{
		"promo_code": "BTT012",       # the id for the promo code. Has to be a new id
		"promo_value": 20,            # monetary value for that promo code
		"promo_used": 12,             # count of promo code usage
        "promo_max_usage": 50,        # maximum count of promo code usage
        "promo_valid": True,          # whether promo code is active
        "promo_multiple_usage": False # whether promo code can be used more than once by a user
	}
	```
	


### Admin --> Update Promo Code :white_check_mark:
Update the values of an existing promo code 
	
!!! put_req "`PUT /admin/promo_codes/:promoId`"
	```promoID: string```: the Id of the promo code to Update.
		This id is a globally unique identifier for that promo code. 

	##### Request
	```JSON
	{
		"promo_value": 20,            # monetary value for that promo code
        "promo_max_usage": 50,        # maximum count of promo code usage
        "promo_valid": True,          # whether promo code is active
        "promo_multiple_usage": False # whether promo code can be used more than once by a user
	}
	```
	


