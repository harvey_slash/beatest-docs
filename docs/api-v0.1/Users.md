# Users
A user is the core actor in Beatest. He can perform actions like purchasing
Tests, Attempting Tests,etc. 

For most actions, a User needs to be logged in. 

A user may log in by supplying his Email/Password, or by 3rd party methods such
as Facebook or Google.

A User may have additional roles associated with him (such as Admin)

## Database Table
Users are currently represented in the database like below

| Field Name           | Data Type                           | Description                                           |
| -------------------- | ----------------------------------- | ----------------------------------------------------- |
| id                   | `int`                               | Unique Identifier for User                            |
| full_name            | `varchar(50)`                       | Full Name                                             |
| fb_id                | `varchar(255), Unique`              | Facebook ID                                           |
| google_id            | `varchar(50)`                       | Google ID                                             |
| fb_oauth_token       | `varchar(500)`                      | Facebook OAuth Token                                  |
| google_oauth_token   | `varchar(500)`                      | Google OAuth Token                                    |
| google_token_id      | `varchar(4000)`                     | Google Token ID                                       |
| email                | `varchar(50), Unique`               | Email                                                 |
| password             | `varchar(500)`                      | Hashed Password                                       |
| profile_picture      | `LONGTEXT`                          | Base64 String                                         |
| referral_code        | `varchar(50)`                       | Referral Code for User                                |
| referral_bonus       | `int`                               | Referral Bonus received during SignUp                 |
| referral_code_used   | `varchar(50)`                       | Referral Code used during SignUp                      |
| wallet               | `int`                               | Wallet Amount (includes refunds and Referral Bonus)   |
| phone_no             | `int`                               | Phone Number                                          |
| type                 | `varchar(50)`                       | UserTypes Enum: Standard, Admin                       |
| is_active            | `tinyint(1)`                        | Activation Mail acknowledged or not                   |
| college_id           | `int (FK to id in college table)`   | College that the User is associated with              |


## API Reference

###  Get User Details :white_check_mark:
Get details of logged in User. This is useful for the client to display information about 
the user in the UI. 


!!! get_req "`GET /user`"

    ##### Response
    ```JSON
    {
        "college": {
        "id":12,
        "name":"Demo College",
        },                             # College object that the user is associated with (may be null)
        "id": 13,                      # PK / Unique Identifier
        "name": "Ram",                 # User Full Name
        "profile_picture": null,       # Profile Picture - Base64 String
        "referralBonus": 50,           # Referral Bonus
        "referralCode": "AZJASP17",    # Referral Code
        "wallet": 0                    # Wallet Balance
        "corporate": {                 # null if user is not a corporate admin
            "id": 1,
            "name": "Apple Inc.",
            "tests": [                 # list of tests that a corporate is allowed to monitor
              {
                "allow_section_jumps": true,
                "character": "Mock",
                "created_date": "2016-11-22T22:43:16",
                "id": 1,
                "is_active": true,
                "leaderboard_id": null,
                "logo": "blobs/Test_icons/a_b_c_d_e_f.png",
                "name": "BEAT CAT 01",
                "price": 0,
                "type": "CAT"
              },
              {
                "allow_section_jumps": true,
                "character": "Mock",
                "created_date": "2016-11-22T22:43:17",
                "id": 2,
                "is_active": true,
                "leaderboard_id": null,
                "logo": null,
                "name": "BEAT IBPS 01",
                "price": 19,
                "type": "IBPS"
              },
              {
                "allow_section_jumps": true,
                "character": "Mock",
                "created_date": "2017-07-15T07:20:32",
                "id": 105,
                "is_active": true,
                "leaderboard_id": null,
                "logo": null,
                "name": "Beat Placement #2",
                "price": 0,
                "type": "COLLEGE"
              },
              {
                "allow_section_jumps": false,
                "character": "Mock",
                "created_date": "2018-09-08T11:07:00",
                "id": 170,
                "is_active": true,
                "leaderboard_id": null,
                "logo": null,
                "name": "Beat Placement CTS #1",
                "price": 0,
                "type": "CAT"
              },
              {
                "allow_section_jumps": false,
                "character": "Mock",
                "created_date": "2018-11-10T15:52:00",
                "id": 180,
                "is_active": true,
                "leaderboard_id": null,
                "logo": null,
                "name": "Beat Wipro National Elite Mock",
                "price": 0,
                "type": "CAT"
              }
            ]
          },

    }
    ```
    
### Login :white_check_mark: :computer:


Logs a User in. This endpoint is supposed to be for cases 
when the email and password is supplied.

If a user is a corporate admin, then an additional value in the session 
`corporate_id` with value equal to the id of the corporate is also sent.

!!! post_req "`POST /user/login`"

    ##### Request
    ```JSON
    {
    "email": "test@b.com",     # User Email Address
    "password": "test123"      # User Password
    }
    ```
    ##### Response
    ```JSON
    {

    "college": {
    "id":12,
    "name":"Demo College",
    }                              # College object that the user is associated with
    "id": 13,
    "name": "Ram",
    "profile_picture": null,
    "referralBonus": 50,
    "referralCode": "AZJASP17",
    "wallet": 0
    }
    ```

### Signup :white_check_mark:

Create a new user. 

This endpoint will create a new row in the User table and requires an email
as an input (Unlike other methods like Facebook Login).



!!! post_req "`POST /user/signup`"

    ##### Request
    ```JSON
    {
        "name": "Ram",          # User Full Name
        "email": "test@b.com",  # Email Address(Must be something that doesnt already exist)
        "password": "test123",  # Password for Account
        "phone_no": 9812129001, # User Phone Number
        "wallet": 0,            # Wallet Balance (unless Referral is used)
        "college_id": 12         # If it is Null, User has not College Association
        "captcha_token": "a87s6df5a8s76df58as764afd" # captcha token from ReCAPTCHA (See Captcha for details)
    }
    ```

### User Activation :white_check_mark:

In order to verify that the user has access to an email, 
a validation email is sent to the email that was used when the user
registered. 

That validation email contains a link to this endpoint, along with a
hashed version of the email (signed using a secret key).


!!! get_req "`GET /user/activate/<activationToken>`"
    `activationToken`  → hashed Email of User 

    ##### Response
    This endpoint returns an `HTTP 304`, with a redirect URL to 
    [http://beatest.in](http://beatest.in)


### Facebook Login :red_circle:

Users may log in using Facebook. 
The following is our process of handling Facebook login.

```diagram
sequenceDiagram
participant client
participant server
participant facebook
client->>facebook: Request Login using Facebook
facebook-->>client: Get Access Token 
client->>server: Send Access Token from Facebook
server->>facebook: Validate Access Token
Note over server: If Token Validated
server-->>client: Log user in 
Note over server: Else
server-->>client: Return 403 Unauthorized
```

The token received from the client is validated by the server. 
Validation involves sending the token to Facebook, and getting the details.
If the Token was issued for Beatest, then it is valid, else it is not. 

The validation step also returns a `user_id` field , which is Facebook's 
unique identifier for that user. 

The Beatest server creates a new User row if a row with `fb_id` equal to the 
returned `user_id` does not exist. This new user's row is set to active. 

After performing the above steps, this endpoint will assign a session cookie 
to the user. This cookie is similar to the one assigned if the user logs in 
using email.

!!! post_req "`POST /user/facebook_login/<activation_token>`"
    `activation_token`  → the token received from facebook 

### Google Login :red_circle:

Users may also log in using Google. 
The logical flow for Google Login is the same as Facebook login, and the 
diagram will be omitted here.

The token received from the client is validated by the server. 
The validation step is also similar to that of Facebook's.

The validation step  returns a `user_id` field , which is Google's 
unique identifier for that user. 

The Beatest server creates a new User row if a row with `google_id` equal to the 
returned `user_id` does not exist. This new user's row is set to active. 

After performing the above steps, this endpoint will assign a session cookie 
to the user. This cookie is similar to the one assigned if the user logs in 
using email.

!!! post_req "`POST /user/google_login/<activation_token>`"
    `activation_token`  → the token received from google 

### Forgot Password :white_check_mark:

In case the User has forgotten his password, the client can call this endpoint
to send an Email to his Email.

A Link will be emailed to the User. The User can visit this link to reset 
his password. (See Reset Password for more information).

**This Link will redirect to a Page in the UI.**

!!! post_req "`POST /user/forgot_password`"

    ##### Request
    ```JSON
    {
    "email": "some_email@fakemail.com",
    "captcha_token": "a87s6df5a8s76df58as764afd" # captcha token from ReCAPTCHA (See Captcha for details)
    }
    ```
    ##### Response
    `HTTP 200` <br/>
         The server sends an email to the specified email id
     with a link to reset password. It will be in the form:
     http://beatest.in/reset_password?token=RESET_TOKEN <br/> <br/>
     **NOTE** This is not a link to an API endpoint ,but 
     a link to a UI endpoint (see Reset Password for more details).

### Reset Password :white_check_mark:

When the Visits the Link obtained from *Forgot Password* , the client should 
display a page with new password fields.

If the User submits the form with those fields, the client should call 
this endpoint. 


!!! post_req "`POST /user/reset_password/<Reset_Token>`"
    `Reset_Token` → The token that was emailed to the User

    ##### Request
    ```JSON
    {
    "new_password": "test321",
    }
    ```

### Change Password :white_check_mark:

If the User would like to change is password , the client can call this endpoint.

**Note** This is different from Reset password. In this case, the User needs to
be logged in before performing this action.

!!! post_req "`POST /user/change_password/`"
    ##### Request
    ```JSON
    {
    "old_password": "old_pass123", # This needs to match current password
    "new_password": "new_pass431", # Some new Password 
    }

    ```

### Resend Activation Mail :red_circle:

If for some reason, the verification email was not received by the User, he may
request to be sent another one.

!!! post_req "`POST /user/resend_activation`"

    ##### Request
    ```JSON
    {
    "email": "someemail@fakemail.com",
    "captcha_token": "a87s6df5a8s76df58as764afd" # captcha token from ReCAPTCHA (See Captcha for details)
    }
    ```

### User Logout :white_check_mark:
Logs a User out. Should clear cookies. 

!!! post_req "`POST /user/logout`"
