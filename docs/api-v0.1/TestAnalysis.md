
# Test Analysis 
# Incomplete Article

After a User finishes a Test, he is allowed to view the answers of the Test and 
compare it to his own responses.


## API Reference

### Get Solutions for Section :red_circle:
Get all the information for a section. 

This will return : 

* All Questions for the section, including html
* Choices for the question (if the question has choices), including html
* Question attempts for the **Latest** Test Attempt for the user.
 
**This response is cached**

!!! get_req "`GET /tests/<testID>/sections/<sectionID>/solutions`"
    `testID` --> the Id test 

    `sectionID`--> the Id section

	##### Response
    A `Test` Object with the following additional keys:

    - `test_attempts` --> array with exactly 1 `Test Attempt` object

        - `section_attempts` --> array with exactly 1 `Section Attempt` object.<br/>
        This section attempt's `section_id` will be equal to the `sectionID` provided
        in the request

    - `Sections` --> array with exactly 1 `Section` object
    
        - `questions` --> array of `Question` objects
            - `choices` --> array of `choice` objects. <br/>
            If the question is not `MCQ` type, then this array will be empty.
    

	```JSON
	{
		"promo_value": 20,    # monetary value for the promo code
	}
    ```


<!-- ### Admin -\-> Get Promo Code Details :white_check_mark: -->
<!-- Get details of a particular promo code  by ID -->

<!-- !!! get_req "`GET /promo_codes/:promoId`" -->
<!-- 	```promoID: string```: the Id of the promo code to get details for. -->
<!-- 	This id is a globally unique identifier for that promo code.  -->
	
<!-- 	##### Response -->
<!-- 	```JSON -->
<!-- 	{ -->
<!-- 		"promo_code": "BTT012",       # the id that was queried for -->
<!-- 		"promo_value": 20,            # monetary value for that promo code -->
<!-- 		"promo_used": 12,             # count of promo code usage -->
<!--         "promo_max_usage": 50,        # maximum count of promo code usage -->
<!--         "promo_valid": True,          # whether promo code is active -->
<!--         "promo_multiple_usage": False # whether promo code can be used more than once by a user -->
<!-- 	} -->
<!-- 	``` -->


<!-- ### Admin -\-> Create PromoCode :white_check_mark: -->
<!-- Register a promo code offer by giving the details. -->

<!-- !!! post_req "`POST /admin/promo_codes`" -->
<!-- 	##### Request -->
<!-- 	```JSON -->
<!-- 	{ -->
<!-- 		"promo_code": "BTT012",       # the id for the promo code. Has to be a new id -->
<!-- 		"promo_value": 20,            # monetary value for that promo code -->
<!-- 		"promo_used": 12,             # count of promo code usage -->
<!--         "promo_max_usage": 50,        # maximum count of promo code usage -->
<!--         "promo_valid": True,          # whether promo code is active -->
<!--         "promo_multiple_usage": False # whether promo code can be used more than once by a user -->
<!-- 	} -->
<!-- 	``` -->
	


<!-- ### Admin -\-> Update Promo Code :white_check_mark: -->
<!-- Update the values of an existing promo code  -->
	
<!-- !!! put_req "`PUT /admin/promo_codes/:promoId`" -->
<!-- 	```promoID: string```: the Id of the promo code to Update. -->
<!-- 		This id is a globally unique identifier for that promo code.  -->

<!-- 	##### Request -->
<!-- 	```JSON -->
<!-- 	{ -->
<!-- 		"promo_value": 20,            # monetary value for that promo code -->
<!--         "promo_max_usage": 50,        # maximum count of promo code usage -->
<!--         "promo_valid": True,          # whether promo code is active -->
<!--         "promo_multiple_usage": False # whether promo code can be used more than once by a user -->
<!-- 	} -->
<!-- 	``` -->
	

