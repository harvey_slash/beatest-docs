# Section Attempts
A Section Attempt belongs to a [TestAttempts](Test Attempt) and has 
multiple [QuestionAttempts](Question Attempts).


## Database Table
SectionAttempts are currently represented in the database like below

| Field Name      	 | Data Type                                                   	 | Description                           	            |
|----------------------|------------------------------------------------------------------|-------------------------------------------------------|
| id              	 | `int (PK)`                                                  	 | Unique Identifier for SectionAttempts 	            |
| section_id      	 | `int (FK to id in `[Sections](Sections.md)` table)`         	 | Id of the section                            	     |
| test_attempt_id 	 | `int (FK to id in `[TestAttempts](TestAttempts.md)` table)` 	 | id of the test attempt                           	 |
| time_left       	 | `int`                                                       	 | Time in Seconds                       	            |
| is_complete     	 | `boolean`                                                   	 | Section Completeness Status           	            |

#### Important Note
The `time_left`   actually  storing the amount of time spent on the section.
The database column is still called `time_left`,but our models use the name
`time_spent`. 

This will be fixed in the future.

## API Reference



### Finish  a Section :red_circle:

Mark a section attempt as complete. Should only be called on a section that does
not allow sectional jumps (e.g. _CAT_ type exams)

Once the section is marked complete , the will be unable to make any attempts on the 
questions under this section.

This endpoint will grant a new **Pinger cookie** with a new timestamp. The test id 
remains the same as the test id of the current Pinger cookie. The section id of the 
pinger cookie is set to the *next section's id* 
(this is required for the user's attempt on the new section to be considered valid.

If the section to be finished was the last section in the test, the pinger cookie
will be left untouched. At this point, there is no use for pinger as no section (and its questions)
will be modifyable.



!!! custom_req " ```POST /tests/<testID>/sections/<sectionID>/attempts/finish```"
	`testID` → The ID of the test that the section belongs to
	`sectionID` → The ID of the section to begin section atempt for
    
    The sectionID and testID field will be ignored from the request.
    The keys sent in the **Pinger cookie** will be used instead
    

### Admin → Delete a Section Attempt

Delete a Section Attempt in the system. Only to be performed by an admin.

It is very unlikely that this this endpoint will be called.


!!! del_req "```DELETE /admin/tests/section_attempts/:sectionAttemptId```"
