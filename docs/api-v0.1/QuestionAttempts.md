# Question Attempts
Question Attempts store the answer and related information
about a particular User on a particular Question. 

A Question attempt has 1 Question and 1 Section Attempt associated with it.


## Database Table
Tests are currently represented in the database like below

| Field Name         | Data Type                                                                      | Description                                                                        |
|--------------------|--------------------------------------------------------------------------------|---  (add-hook 'python-mode-hook' (lambda ()
                                 (fci-mode 1)
                                 (evil-close-folds)))
---------------------------------------------------------------------------------|
| section_attempt_id | `int (FK to id in `[Section Attempt](SectionAttempts.md)` table),(part of PK)` | The section attempt that this question attempt is part of                          |
| question_id        | `int (FK to id in `[Question](Questions.md)` table),(part of PK)`              | The question for which this is an attempt                                          |
| choice_id          | `int(FK to id in `[Choice](Choices.md)` table)`                                | The choice of the user (in case of MCQ only ,otherwise `NULL`)                     |
| tita_choice        | `varchar(50)`                                                                  | The TITA answer of the user (in case of TITA question type only, otherwise `NULL`) |
| attempt_status     | `varchar(50)`                                                                  | See below
| time_spent         | `int`                                                                          | Total time spent on the question                                                   |




`attempt_status` types

| Value    | Description                                               |
|----------|-----------------------------------------------------------|
| `null`     | (default) question was neither seen nor marked for review |
| `'seen'`   | question was seen by the user                             |
| `'review'` | question was marked for review                            |

The 'attempt_status' is just used to store user's action. 
Whether or not the user has attempted a question depends on his `choice_id` and `tita_choice` value. 
If they are `null` that question will be ignored during score calculation.



## API Reference




### Attempt a Question  :white_check_mark: Submit an attempt on a Question

This will overwrite any previous attempts made on that question.

**This requires that the Section Attempt and Question Attempt for the Question 
to be already created**

If the section attempt or test attempt of this question attempt is marked complete, 
this endpoint will **fail**.

If the test type allows sectional jumps, then only the testID(it has to match the testID in the request URL) and time of last ping
will be checked from the cookie. If it does not allow sectional jumps, then section ID will also be checked.

!!! put_req " ```PUT /tests/<testID>/sections/<sectionID>/questions/<questionID>/attempts```"

	`testID` → The ID of the test that the question belongs to

	`sectionID` → The ID of the section the question belongs to

    **Note** The above fields will be ignored. Instead, the data from **pinger's cookie** will be used. 
    The above data is used for logging purposes.

	`questionID` → The ID of the question to submit an attempt for 

		

    ##### Request

    This endpoint requires 


	```JSON
	{
		"choice_id": 12,     # the choice id   (only for MCQ type questions)
		"tita_choice": 12    # the TITA answer (only for TITA type questions)
	}
	```


