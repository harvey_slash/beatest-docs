# Sections
Sections are part of tests. Each Section belongs to a [Test](Tests.md), and has many 
[Questions](Questions.md).


## Database Table
Sections are currently represented in the database like below

| Field Name   | Data Type                                    | Description                                                   |
|--------------|----------------------------------------------|---------------------------------------------------------------|
| id           | `int (PK)`                                   | Unique identifier for Section                                 |
| test_id      | `int (FK to id in `[Test](Tests.md)` table)` | Which test a section belongs to                               |
| name         | `varchar (50)`                               | Name of section                                               |
| created_date | `datetime    `                               | When it was created (by the admin)                            |
| total_time   | `int `                                       | Maximum time allowed by the user in this section (in seconds) |


## API Reference

### List Sections for Test :white_check_mark:

Get Information about all the Sections under a particular Test.

Sections can only be listed if there exists a Test Attempt by a User
which is not complete yet. 

!!! get_req "```GET /tests/:testID/sections```"
    `testID` The id of the test to list sections of

	##### Response [1]
	```JSON
    [
        {
          "created_date": "2017-03-27T18:10:56",
          "id": 73,
          "name": "Logical Reasoning",
          "test_id": 59,
          "total_time": 3600
        },
        {
          "created_date": "2017-03-27T18:15:41",
          "id": 74,
          "name": "Quantitative Aptitude",
          "test_id": 59,
          "total_time": 3600
        },
        {
          "created_date": "2017-03-27T18:16:04",
          "id": 75,
          "name": "English Language",
          "test_id": 59,
          "total_time": 3600
        }
    ]

	```
    
	##### Response [2]
	If there is no test attempt for the test by the user, this endpoint returns an
	Error with `HTTP Code: 403`


### Admin → Add a Section 

Add a Section to the system. Only to be performed by an admin. 

This requires the associated Test to already have been created.  

By adding a Section, it only creates a row in the Section table, so 
questions belonging to the section
should be added after calling this endpoint.
 
 
!!! post_req "```POST /admin/tests/:testID/sections```"
	`testID` the id for the test to add this section under
	 
	##### Request
	```JSON
	{
		"name": "section1",                     
		"total_time": 500
		
	}
	```
	**Note:** The `created_date` will be added automatically by the system
	##### Response
	```JSON
	{
		"id": 12    # the newly created id for the section
	}
	```


### Admin → Update a Section

Update a Section to the system. Only to be performed by an admin.

By updating a Section, it updates the row in the Section database, so
Questions belonging to this Section
can't be modified using this endpoint.


!!! put_req "```PUT /admin/tests/sections/:sectionID```"
	`sectionID` -- The id of the section to modify
	
	**Note** The testID was omitted from the URL because the sectionID is a globally unique ID  . 
	Adding testID would be redundant, because a section belongs to exactly 1 Test.
	
	##### Request
	```JSON
	{
		"name": "section2",                     
		"total_time": 553
	}
	```
    

### Admin → Delete a Section

Delete a Section in the system. Only to be performed by an admin.

By deleting a Section, it deletes the row in the Section table, and
**all the questions associated with that section.**  

It is very unlikely that this this endpoint will be called. 


!!! del_req "```DELETE /admin/tests/sections/:sectionID```"
    
