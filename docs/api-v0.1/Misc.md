# Miscellaneous

This page documents APIs that do not belong to any particular
category.





### Contact Us :white_check_mark:

Users from the home page can send a contact request. 

This endpoint just takes in the input of the forms and sends 
an Email to the admins. 


!!! post_req "`POST /misc/contact_us`"

	##### Request
	```JSON
	{
	    "name":          "Ben Dover",
	    "email":         "ben_dover@gmail.com",
        "message":       "How do I do this thing?",
        "captcha_token": "as8d7f69a8s7d6f"
	}
	```

### Admin → Promo Code Metrics :white_check_mark:

In order to monitor the status of [Promo Codes](PromoCodes.md) 
this endpoint may be used. 

It returns **HTML** in the form of a table and is meant 
to be visited in the browser itself.

This endpoint is a quick hack and will be removed soon.

!!! post_req "`GET /admin/misc/promo_metrics`"

