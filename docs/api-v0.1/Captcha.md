

Publicly accessible endpoints that perform expensive tasks (such as sending mail)
need to be protected from malicious automated attacks.

In order to allow this , we use [ReCAPTCHA](https://www.google.com/recaptcha/intro/)


### ReCAPTCHA Validation Flow 

Along with the data required by the endpoint itself, 
the token received by calling `grecaptcha.getResponse()` should also be sent to 
Beatest.

The ReCAPTCHA Flow is as follows:

```diagram
sequenceDiagram
participant Client
participant Beatest
participant ReCAPTCHA Server

Client->>Beatest:  Send ReCAPTCHA token along with other data

Beatest->>ReCAPTCHA Server:  Request verification status
ReCAPTCHA Server-->>Beatest: Return verification status
Note over Beatest:           if status is 'valid'
Beatest->>Beatest :          Continue with Endpoint Logic
Beatest-->>Client:           return endpoint response
Note over Beatest:           else
Beatest-->>Client:           return Error 
```

## Captcha Standards

For ALL endpoints that require captcha , the token must be sent under the key 
`captcha_token`. The endpoint that require captcha will fail if this key is not
present.

Endpoints Using ReCAPTCHA:

-   [User Signup](Users.md#signup) 
-   [Forgot Password](Users.md#forgot-password) 
-   [Resend Activation Mail](Users.md#resend-activation-mail) 
-   [Contact Us](Misc.md#contact-us) 
