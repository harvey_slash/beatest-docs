# Colleges APIs

We mantain college specific tests that is displayed on the users'
"college profile". See the college profile endpoint below for more details.




## Database Table
Colleges will be represented in the database like below

| Field Name           	| Data Type       	| Description                   	|
|----------------------	|-----------------	|-------------------------------	|
| id                   	| `int (PK)`      	| Unique Identifier for College 	|
| college_name         	| `varchar (100)` 	| College Name                  	|
| college_logo         	| `varchar (100)` 	| College Picture S3 Link       	|


## API Reference

### Get Colleges :white_check_mark:
Get list of Colleges registered with Beatest

!!! get_req "`GET /colleges`"
	##### Response
	```JSON
	[
        {
            "id": 3,
            "college_name": "Beatest Institute",
        },
        {
            "id": 4,
            "college_name": "Beatest Institute",
        }
	]
	```

### List Tests for College :red_circle:
Get all [Tests](Tests.md) that are part of a College.

This endpoint is supposed to find the Test ids only 
(actual Test details can only be accessed after purchase)

!!! get_req "`GET /colleges/<collegeID>/tests`"
    `id` --> identifier for the college 
	##### Response
    An array of Test objects.
	```JSON
	[
        {
            "id": 3,
        },
        {
            "id": 4,
        }
	]
	```
### Create College :white_check_mark:
Register a College with Beatest

!!! post_req "`POST /admin/colleges`"
    ### Request
	```JSON
	{
        "college_name": "Beatest Institution",
        "college_logo": "https://mybucket.s3.amazonaws.com/myfolder/afile.jpg"
    }
	```
	##### Response
    ```JSON
    {
        "id": 10,
        "college_name": "Beatest Institution",
        "college_logo": "https://mybucket.s3.amazonaws.com/myfolder/afile.jpg"
    }
    ```

### Update College :white_check_mark:
Update a College with Beatest

!!! post_req "`PUT /admin/colleges`"
    ### Request
	```JSON
	{
        "college_name": "Beatest Institution",
        "college_logo": "https://mybucket.s3.amazonaws.com/myfolder/afile.jpg"
    }
	```
	##### Response
    `HTTP 200` Update Successful <br/>
    `HTTP 400` Update failed due to Bad Parameters

### College Profile Tests :red_circle:

Return a list of tests to be displayed in the users college profile.

1. If the user is not logged in , all tests with `college_id` ==`NULL` (in the `college_test` table) will be returned. 
2. If the user is logged in but has `college_id` set to null, then all tests with `college_id` == `NULL` will be returned (same as above).
3. If the user is logged in and has a college_id , then all tests with that `college_id` will be returned.


The output of this endpoint is exactly the same as the [Get Tests endpoint](/api-v0.1/Tests/#get-list-of-tests)

!!! get_req "`GET /colleges/tests`"
    !!! danger
        See the output of [Get Tests endpoint](/api-v0.1/Tests/#get-list-of-tests)

    
