# Discourse APIs

In order to have community discussions, we use [Discourse](http://discourse.org).

We use Discourse SSO (Single Sign On) to use discourse while only allowing 
users to sign in via Beatest.



### Discourse SSO Flow 

In order to allow a user access to restricted content on Discourse, we need to first
authenticate him on Beatest. 

The SSO flow is as follows:

```diagram
sequenceDiagram
participant Discourse
participant Client
participant Beatest

Discourse->>Client:  Redirect to Client's SSO page with query params
Client->>Client:     If User not logged in, prompt for details and log in

Client->>Beatest:    forward query parameters to beatest SSO endpoint 
Beatest->>Database:  verify user and check for course access
Beatest->>Client:    return URL with signed payload
Client-->>Discourse: redirect user to returned URL
```

### Integration with Discourse

In order to sync data between Discourse and Beatest, we currently use the
following Discourse hooks/APIs.

- SSO Login (See Below)
- Add User to Group.
`PUT https://forums.beatest.in/groups/{group_id}/members.json`
There are forum subcategories that require a User to purchase certain items. 
When the purchase is made, the api is used to allow the user access to that
subcategory.

### Discourse API Reference

### SSO Login :white_check_mark:

In order to verify that a User has access to restricted groups, and also 
verify if he has an account on Beatest, a special SSO endpoint exists. 

!!! post_req "`POST /discourse_sso`"

    ##### Request
    During redirect from Discourse to the Webpage, query parameters with 
    signed data will be sent. 
    These query parameters will need to be forwarded to this endpoint 
    without any tampering. 


	##### Response
	```JSON
	{
	    "url": "https://forums.beatest.in/session/sso_login?..."  # A long URL
	}
	```

After the URL is returned from this endpoint, the Client must redirect to that URL. 
This will take him to the forum and give him access to the contents that he paid for.
