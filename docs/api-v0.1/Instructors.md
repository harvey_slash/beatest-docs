

# Course APIs

Courses are products that are offered by Beatest. A user can buy courses using our website.
The course is accessible via some 3rd party software. Beatest server provides the mechanism
to allow a user to Purchase the course.



# (Incomplete)
## Database Table :white_check_mark:

| Field Name   | Data Type      | Description                        |
|--------------|----------------|------------------------------------|
| id           | `int (PK)`     | Unique identifier for Course       |
| name         | `varchar (50)` | Name of Course                     |
| is_active    | `tinyint(1)  ` | Is the Course active               |
| price        | `int(11)     ` | Price of a Course                  |
| created_date | `datetime    ` | When it was created (by the admin) |
| updated_date | `datetime    ` | When it was updated (by the admin) |




## API Reference


### Get List of Courses :white_check_mark:

Get information about all Courses in the system. 
If User has purchased some courses, this endpoint will also 
specify that.

**This will only list active courses**

!!! get_req "```GET /tests```"
    if the user has purchased a Course , `is_purchased` 
    will be true for that course.
	
	**`is_purchased` will be `false` for not logged in Users**
	
	##### Response
	```JSON
	[
		{
			"id": 12,                      # test id is globally unique
			"name": "Web Development 1",   # name of the course
			"is_purchased": false,         # whether or not the user has purchased this test 
                                           # (this is an artificially generated field) 
			"price": 99,
		},
		{
			"id": 12,                      # test id is globally unique
			"name": "Big Data",            # name of the course
			"is_purchased": true,          # whether or not the user has purchased this test 
                                           # (this is an artificially generated field) 
			"price": 99,
		},
	]
	```

