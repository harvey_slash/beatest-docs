# Welcome to API Design Guidelines
We will follow the general naming conventions of REST. We will not follow all the standards of REST. 
To get REST information , visit https://restfulapi.net




## API Versioning and Namespacing

All Apis should be under their version number, and throughout these docs, that will be **omitted**. 
But it is be assumed that correct versioning is required in order to get expected results. 

E.g. When the docs refer to an api such as `/test/`, the client has to visit 

```JSON
/api/v0.0/test
```


## API Naming Convention

In general , the REST verbs (GET, POST, DELETE) should be used , if unambiguity is possible.

## Error Messages
Whenever possible, HTTP Level status codes must be used to signifiy the problem (E.g `403` for unauthorized access)
Additionally, we define our own error message structure so that all clients can expect a unified response. 

```JSON 
{
	"message": "No tests left",   # description of error. Can be displayed to the UI directly
	"error_code": 23408,          # unique identifier of the error (optional)
	"additional": {},             # placeholder for extra data (optional)
}
```


## Examples 

### Get a list of all tests

!!! get_req "`GET /test`" 
	##### Response
	```JSON
	{
		"id": 12,
		"name": "test1"
	}
	```
	
### Add a test

!!! post_req "`POST /tests`"
	##### Response
	```JSON
	{
		"id": 32
	}
	```


!!! warning "We did NOT use"
	`GET /test/add`
	
	Because it was possible to do the same by using the `POST ` verb. 
	
!!! danger "Under NO circumstances should `GET` verb be used to perform any operation that modifies data"
	It is only meant for operations that fetch data
	
	This is to prevent issues that CSRF attacks can create
	
	Read about CSRF attacks here https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)
	
	



### Custom Endpoints

If the action to be performed is not a simple CRUD (Create,Read,Update,Delete), the following convention must be followed: 

```POST /resource/sub-resource/action_name```

### To activate a user

!!! custom_req "`POST /user/activate/32`"
	##### Response
	```JSON
	{
		success: True
	}
	```
	
### Status Codes

The HTTP status code should be used. HTTP code standards will be utilized to convey the status of request. 
Read about them at https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html 


### Blank requests or responses

If there is no additional information to be conveyed from a particular request's body, or from the response data, they will be 
omitted from the api blocks. 


E.g. 

### To get tests 
The request was omitted

!!! post_req "`POST /user/activate/32`"
	##### Response
	```JSON
	{
		"id":21,
		"name":'some name'
	}
	```

### To activate a user
The request as well as response was omitted

!!! post_req "`POST /user/32/activate`"
	




!!! tip "Creating Resources in `POST` calls"
	Most of the time, when a resource is created using a `POST` call, 
	the database creates an ID for that resource. 
	
	Remember to send that ID back , if the client may require it in the future


## File Uploads

We follow a specific pattern to handle endpoints that need require 
file uploads. See [File Uploads](FileUploads.md) 
## Admin Tasks 

If there is any endpoint that is supposed to be accessible only by an admin (E.g. adding a test),
it should be namespaced by `admin`. 
**It is assumed that all endpoints namespaced by `admin` will fail unless the user issuing that request
is a verified admin.**

Example

### Add a promo code

!!! post_req "`POST /admin/promo_codes/`"
	##### Response
	```JSON
	{
		"id":32
	}
	```


