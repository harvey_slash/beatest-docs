
# Time Tracking

In order to capture the amount of time spent on a Question,
   is is neccessary to send information from the client
  to the the server periodically. This allows the server
 to update the time spent on each Question/Section. 


## Heartbeat Signal :white_check_mark:
 A signal with the required information is sent every 20 seconds to the server.
 To prevent tampering, signed cookies are transferred to and from the client.
 
 
The pinger will only perform the time update operation if it receives a cookie 
that has time of last ping <= 20 seconds. 

When a successful ping happens, the time update operation is updated to the current
system time of the server. 


In order to be able to bootstrap this proccess, an initial time of last update needs 
to be set. The following endpoints can send a cookie with time of issue: 

* Start/Resume Test 
* Start/Resume Section 


The following endpoints need the time of issue to be <= 20 seconds in order to proceed: 

* Update Question Attempt
* Submit section 


```diagram
sequenceDiagram
participant client
participant server
participant database
loop every 20 seconds
    client->>server:   Heartbeat Signal 
    server->>database: update time spent on section
    server->>database: update time spent on test
    server-->>client:  update cookie with new last ping timestamp
end
```

## Update Time :white_check_mark:
Performs the Heartbeat signal

Updates the time spent for a particular question. 
Behind the scenes , it will also update the time spent for the 
Section that the Question belongs to. 

If the test doesnt allow sectional jumps, it will automatically 
set the *is_complete* flag of the section to False.



**This requires the question attempt for the question id to already exist**



!!! post_req "```POST /tests/<testID>/sections/<sectionID>/questions/<question_ID>/attempts/ping```"
        
        `testID`  -->   The ID of the current test.

        `sectionID`  -->   The ID of the Section to update time for.

        `question_ID`  -->   The ID of the Question to update time for.



