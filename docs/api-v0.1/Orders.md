# Orders APIs
An Order is the fundamental method with which a User can buy products from 
Beatest. We use Razorpay as our Payment provider.

## Database Table
Colleges will be represented in the database like below

| Field Name             | Data Type         | Description                                 |
| ---------------------- | ----------------- | -------------------------------             |
| id                     | `int (PK)`        | Unique Identifier for Order                 |
| rp_order_id            | `string, Unique`  | Razorpay Order ID                           |
| status                 | `(Enum)`          | Razorpay Order Status                       |
| amount                 | `Float`           | Order Amount (After promo code was applied) |
| promo_code_id          | `int`             | FK to id in Promo Code table                |
| user_id                | `int`             | FK to id in User table                      |
| created_date           | `datetime`        | Order Creation date                         |
| updated_date           | `datetime`        | Order Update date (Status changes)          |

**```Status Type```**

| Value         | Description                                               |
|---------------|-----------------------------------------------------------|
| created       | Order has been freshly created                            |
| attempted     | Payment was attempted, but did not succeed                |
| paid          | Payment successfully fulfilled                            |
| cancelled     | Payment was cancelled (Can only be cancelled if not Paid) |


## Order Process

Ordering a collection of Items involves Two steps: Order Creation and Status Update. 
The Order Creation step is as follows. 
```diagram
sequenceDiagram
participant client
participant Beatest
participant Database
participant RazorPay

client->>Beatest:    Request new Order
Beatest->>Database:  Calculate amount & Create New Order
Beatest->>RazorPay:  Request new Razorpay Order
RazorPay-->>Beatest: Get Unique Razorpay Order id
Beatest->>Database:  Update Order with  Razorpay Order id
Beatest-->>client:  Return Order Id & Razorpay Order id
client->>RazorPay: Make payment using Razorpay Order id
```
After making an Order, the User needs to inform Beatest that he has made a Payment.
Beatest will then check if that is true (if money was successfully transferred).

If it is true, then the Order status will be set to paid.

The Order Status update step is as follows:
```diagram
sequenceDiagram
participant client
participant Beatest
participant Database
participant RazorPay

client->>Beatest: Request Order Status Update
Beatest->>RazorPay: Request Order status Update
RazorPay-->>Beatest: Return Order Status
Beatest->>Database: Update new Order status

Beatest-->>client: Return new Order Status

```
## API Reference


### Get User Orders :white_check_mark:
Get list of Orders for a User. Status based filter is allowed.

!!! get_req "`GET /orders`"

    ##### Supported Query Params
	| Key         | Description                                                |
	| ----------- | ---------------------------------------------------------- |
	| status      | `string` Order's Payment status                            |

	##### Response
    The response structure is the same as 
    `GET /orders/:id`. The difference is that in this case, an array of orders
    will be returned.
	```JSON
	[
        {
            "id": 3,
            "rp_order_id": "order_1928393",
            "status": "created",
            "amount": 100.00,
            "created_date": "23/02/2018",
            "tests": [
                         { "id":21 },
                         { "id":81}
                     ],
            "courses": []
        },
        {
            "id": 31,
            "rp_order_id": "order_19281293",
            "status": "created",
            "amount": 100.00,
            "created_date": "23/02/2018",
            "tests": [
                         { "id":21 },
                         { "id":81}
                     ],
            "courses": []
        }
	]
	```


### Get User Order :white_check_mark:
Get Details for a single User.

!!! get_req "`GET /orders/:id`"
    `id` --> identifier for the order 

    ##### Response
    Apart from standard keys , the response will also contain
    `tests` and `courses`. If the order had no tests or courses,
    that particular key will have an empty array.
    The keys will be an array of objects, with only the `id` key of each object
    present.
	```JSON
	{
        "id": 3,
        "rp_order_id": "order_1928393",
        "status": "paid",
        "amount": 100.00,
        "promo_code_id": 1,
        "user_id": 137,
        "created_date": "23/02/2018",
        "updated_date": "24/02/2018",
        "tests": [
                      { "id":21 },
                      { "id":81}
                  ],
        "courses": []
    }
	```


### Create User Order :white_check_mark:

**Order creation will do the following:**

1. Calculate Total Amount of Items (Tests, Courses, etc.)
2. If [promo code](PromoCodes.md) is provided, validate it, and subtract the amount
associated with it.
3. Create Razorpay Order ID and Insert Order
4. Store Order ID and Item ID in the respective Item Order table


e.g., Test IDs will be stored in OrderTest table against the generated Order ID

!!! post_req "`POST /orders`"

    ##### Info
    Order will be created with Razorpay and passed back to Client. This will be
    used by Client to accept User's payment. Once paid, below endpoint will be
    called by Client to notify server about payment & to do verification.

    ##### Request
    The request requires either the `tests` or `courses` (or both) to be present.
    If they are present, they must be a list containing the ids for the item that 
    the user wants to purchase.

	```JSON
	{
        "tests": [1,2,3,5],
        "promo_code": "BTT01"
    }
	```
	##### Response
    ```JSON
    {
        "id": 1,
        "rp_order_id": "order_1928393"
    }
    ```
    `HTTP 200` Order created <br/>
    `HTTP 400` Order failed - Invalid PromoCode (any other reason?)


### Cancel Order :white_check_mark:

A user has the option to cancel an order that he has placed. 
He can only cancel an Order if it is not marked as paid.


!!! post_req "`POST /orders/:id/cancel`"


### Update Order - Payment Status :white_check_mark:

After the Order is created, the User proceeds to paying using his bank details.
After successful payment , the User inform Beatest server that payment has been
made. Beatest server will then check if thats true , and if it is, the order
status will be set to paid.

###### Info
If status of Order is paid then, User will be able to access all items that he
paid for.

!!! post_req "`PUT /orders/:rp_order_id/status`"
    `rp_order_id` -> the razor pay order id that was received during order 
    creation

	##### Response
	```JSON
	{
	    "status": "paid"  # returns the updated status
	}
	```
If the new updated status is `paid`, a signal to [Discourse](Discourse.md) is sent to allow the user 
access to all the course forums (if any).
