# Tests
Tests make up the core unit of Beatest. They need to be purchased by the user. 
Each test can have many [Sections](Sections.md), and each sections can have many [Questions](Questions.md).

A user may have multiple attempts on the same test, but at any given point 
in time, there can be at **at most 1** active test. 

An active test is a test for which the *is_complete* attribute is *false*.
If there are multiple test attempts by the same user for the same test,
   the *is_complete* attribute of all but 1 must be *true*.


## Database Table 
Tests are currently represented in the database like below

| Field Name       | Data Type                             | Description                                     |
|------------------|---------------------------------------|-------------------------------------------------|
| id               | `int (PK)`                            | Unique identifier for test                      |
| leaderboard_id   | `int (FK to id in leaderboard table)` | **NOT SURE**                                    |
| name             | `varchar (50)`                        | Name of test                                    |
| created_date     | `datetime    `                        | When it was created (by the admin)              |
| is_active        | `tinyint(1)  `                        | Is the test active                              |
| type             | `varchar(50) `                        | If possible to jump between sections(See below) |
| character        | `varchar(50) `                        | Mock or Topic type test (See below)             |
| price            | `int(11)     `                        | Price of a test                                 |
| instruction_html | `longtext    `                        | The instruction html associated with the test   |


#### Test Types
A Test 'type', can be _CAT_ , _College_ or _IBPS_. 

It is possible to jump between sections in any non _CAT_ types exams. There
is also no section time limit for these tests.

For _CAT_, the current section is locked once the time is up (each section has its own time limit), or when the user submits that section. It will not be
accessible after that.

#### Test Character
A Test can have one of two 'characters' : _Mock_ and _Topic_. 

#### Active/Inactive Tests
A Test can be active or inactive. If it is Inactive, a user cannot take the test. It will also not show up to the user. 


## API Reference

### Get List of Tests :white_check_mark:

Get information about all tests in the system. 
Note that this does NOT return Sections or Questions.
It is meant to return general information about tests so that it can be 'browsed' from UI.

**This will only list active tests**

!!! get_req "```GET /tests```"
	##### Supported Query Params
    | Key       | Description                                       |
    |-----------|---------------------------------------------------|
    | type      | `string` the _type_ of the test to filter by      |
    | character | `string` the _character_ of the test to filter by |
	
	**`is_purchased` will be `false` for not logged in Users**
	
	##### Response
	```JSON
	[
		{
			"id": 12,                 # test id is globally unique
			"name": "test1",          # name of the test
			"type": "CAT",   
			"is_purchased": false,    # whether or not the user has purchased this test 
									  # (this is an artificially generated field) 

			"is_complete": false,     # whether or not the user has completed the test
									  # (this is an artificially generated field) 
			"character": "Mock",      # can be mock or topic 
			"price": 99,
            "total_time": 18000,      # total time of test in seconds
                                      # (this is an artificially generated field)
            "section_count": 3,       # number of sections in test
            "quesiton_count": 38,      # number of questions in test
                                      # (this is an artificially generated field)
		},
		{
			"id": 14,                 # test id is globally unique
			"name": "test2",          # name of the test
			"type": "CAT",
			"is_purchased": true,     # whether or not the user has purchased this test 
			                          # (this is an artificially generated field) 
									
			"is_complete": true,     # whether or not the user has completed the test
									  # (this is an artificially generated field) 
			"character": "Mock",      # can be mock or topic 
			"price": 99,
            "total_time": 18000,      # total time of test in seconds
                                      # (this is an artificially generated field)
            "section_count": 3,       # number of sections in test
            "quesiton_count": 81,      # number of questions in test
                                      # (this is an artificially generated field)
		}
	]
	```


### Get College Tests :white_check_mark

Get information about all tests specific to colleges in the system. 
Note that this does NOT return Sections or Questions.
It is meant to return general information about tests so that it can be 'browsed' from UI.


If the user is not mapped to a college, all the tests of type COLLEGE from the 
test table will be returned. The conditions for access to those tests are same as other tests. 


If the user is mapped to a college, all the tests that are listed for his college only will be
displayed. 
The user may access that college if: 
1. That test is free (price = 0)
2. That test is free for his college 
3. A payment has been made for that test for the user. 



!!! get_req "```GET /tests/college```"
	
	**`is_purchased` will be `false` for not logged in Users**
	
	##### Response
	```JSON
	[
		{
			"id": 12,                 # test id is globally unique
			"name": "test1",          # name of the test
			"type": "CAT",   
			"is_purchased": false,    # whether or not the user has purchased this test 
									  # (this is an artificially generated field) 

			"is_complete": false,     # whether or not the user has completed the test
									  # (this is an artificially generated field) 
			"character": "Mock",      # can be mock or topic 
			"price": 99,
		},
		{
			"id": 14,                 # test id is globally unique
			"name": "test2",          # name of the test
			"type": "CAT",
			"is_purchased": true,     # whether or not the user has purchased this test 
			                          # (this is an artificially generated field) 
									
			"is_complete": true,      # whether or not the user has completed the test
									  # (this is an artificially generated field) 
			"character": "Mock",      # can be mock or topic 
			"price": 99,
		}
	]
	```


### Get Details of one test :white_check_mark:

Get information about a single test. 
This returns all the information (including `instruction_html`) for the test id.

This requires the user to be logged in and have access to the Test.


!!! get_req "```GET /tests/:testID```"
	`testID` The id of the test.

	
	##### Response
	```JSON
		{
			"id": 12,                 # test id is globally unique
			"name": "test1",          # name of the test
			"type": "CAT",   
            "instruction_html": "<html></html>" # long html of the test
									       
			"character": "Mock",      # can be mock or topic 
			"price": 99,
		}
	```

### Admin → Add a Test:question:

Add a Test to the system. Only to be performed by an admin. 

By adding a Test, it only creates a row in the Test database, so 
sections belonging to the test (and questions belonging to those sections)
should be added after calling this endpoint.
 
 
!!! post_req "```POST /admin/tests```"
	##### Request
	```JSON
	{
		"name": "test1",                      # name of the test
		"type": "CAT",
		"character": "Mock",                  # can be mock or topic
		"price": 99,
		"is_active": true,
		"instruction_html": "<long_html>"
	}
	```
	**Note:** The `created_date` will be added automatically by the system
	##### Response
	```JSON
	{
		"id": 12
	}
	```


### Admin → Update a Test:question: 

Update a Test to the system. Only to be performed by an admin.

By updating a Test, it updates the row in the Test database, so
sections belonging to the test (and questions belonging to those sections)
can't be modified using this endpoint.


!!! put_req "```PUT /admin/tests/:testId```"
	##### Request
	```JSON
	{
		"name": "test1",                      # name of the test
		"type": "CAT",
		"character": "Mock",                  # can be mock or topic
		"price": 99,
		"is_active": true,
		"instruction_html": "<long_html>"
	}
	```
    

### Admin → Delete a Test

Delete a Test to the system. Only to be performed by an admin.

By deleting a Test, it deletes the row in the Test database, and
**all the sections (and all the questions of each section) associated
with that test.**

It is very unlikely that this this endpoint will be called. 


!!! del_req "```DELETE /admin/tests/:testId```"
    
