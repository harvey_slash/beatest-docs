# Test Attempts
Test Attempts record the attempt made by a particular User on a particular Test. 
A User can have **at most 1 Active** Test attempt for a particular Test.


## Database Table
Test Attempts are currently represented in the database like below

| Field Name  | Data Type                                   | Description                          |
|-------------|---------------------------------------------|--------------------------------------|
| id          | `int (PK)`                                  | Unique identifier for test attempt   |
| user_id     | `int (FK to id in `[User](Users.md)`table)` | The user who is attempting this test |
| test_id ** Vargab Bakshi-Shopify [Harsh]
    
** Ekhlaque Bari -- Fullerton [Harsh]
    | `int(FK to id in `[Tests](Tests.md)`table)` | The test that is being attempt       |
| is_complete | `boolean    `                               | Is the test marked complete          |
| score       | `tinyint(1)  `                              | The score obtained on the test       |
| date        | `datetime  `                                | **NOT SURE**                         |




## API Reference

### Begin Attempt on a Test :white_check_mark:

Begin the attempt on a Test. 
This will create a row in the Test Attempts table, and rows 
in the section attempts table and question attempts table. 

The creation of test attempt and section attempt/question attempts is atomic,
and the data is in an inconsistent state if one of these exists without the other. 


**NOTES** 

* This will create the section attempts automatically as well
as question attempts.

* Even though the endpoint is called start, it is possible to call
start test multiple times. 

* If a user has some test attempt that is not complete (*is_complete=False*),
  a new Test Attempt will **not be created**. Instead, that existing test attempt
 will be returned. 
 
* Currently, if a test is marked complete, the user cannot start another attempt.

**This requires a User to have access to the Test**

!!! custom_req " ```POST /tests/:testID/attempts/start```"
	`testID` → The ID of the test to start the test
		




### Get Test Attempt Details

Get the details of Test Attempt. 
This will also return Section Attempts under the test, and question attempts
under each section.



!!! get_req " ```POST /tests/:testID/attempts/start```"
	`testID` → The ID of the test to start the test
		
### Finish a Test  :white_check_mark:

Mark a Test as complete. This is usually done when a user clicks on 'finish test'.
The User will be unable to view or attempt questions after performing this action. 

This also calculates the score for the User's attempt , and stores it in the `score` Field.


**This requires an unfinished Test Attempt to already exist**

!!! custom_req " ```POST /tests/:testID/attempts/finish```"

	`testID` → The ID of the test to finish the test


### Get Performance Data  :white_check_mark:

Get information about user's performance on the test. 
This can only be calculated once he has submitted his test.



!!! get_req " ```GET /tests/:testID/attempts/performance```"

	`testID` → The ID of the test
    
    ##### Response
    
    ```JSON
        {
      "date": "2019-01-15T06:57:27",
      "id": 9564,
      "is_complete": true,
      "is_graded": true,
      "max": 9.0,
      "median": 9.0,
      "min": 9.0,
      "percentile": 100,
      "rank": 1,
      "score": 9,
      "section_attempts": [
        {
          "correct_question_count": 4,
          "id": 23023,
          "incorrect_question_count": 6,
          "is_complete": true,
          "question_attempts": [
            {
              "question": {
                "id": 6979,
                "lod": "Medium",
                "topic": "Synoyms",
                "type": "MCQ"
              },
              "question_id": 6979,
              "section_attempt_id": 23023,
              "time_spent": 20
            },
            {
              "question": {
                "id": 6980,
                "lod": "Easy",
                "topic": "Antonyms",
                "type": "MCQ"
              },
              "question_id": 6980,
              "section_attempt_id": 23023,
              "time_spent": 10
            }
            ],
            
          "score": 4,
          "section": {
            "created_date": "2018-11-10T15:52:00",
            "id": 313,
            "name": "English Comprehension",
            "test_id": 180,
            "total_time": 600
          },
          "section_id": 313,
          "test_attempt_id": 9564,
          "time_spent": 145,
          "total_question_count": 10
        },
        {
          "correct_question_count": 2,
          "id": 23024,
          "incorrect_question_count": 7,
          "is_complete": true,
          "question_attempts": [
            {
              "question": {
                "id": 6989,
                "lod": "Medium",
                "topic": "Puzzles",
                "type": "RC"
              },
              "question_id": 6989,
              "section_attempt_id": 23024,
              "time_spent": 15
            },
            
          ],
          "score": 2,
          "section": {
            "created_date": "2018-11-10T15:53:00",
            "id": 314,
            "name": "Logical Reasoning",
            "test_id": 180,
            "total_time": 600
          },
          "section_id": 314,
          "test_attempt_id": 9564,
          "time_spent": 160,
          "total_question_count": 9
        },
        {
          "correct_question_count": 3,
          "id": 23025,
          "incorrect_question_count": 7,
          "is_complete": true,
          "question_attempts": [
            {
              "question": {
                "id": 6998,
                "lod": "Easy",
                "topic": "Number System",
                "type": "MCQ"
              },
              "question_id": 6998,
              "section_attempt_id": 23025,
              "time_spent": 85
            }
            
            ],
          "score": 3,
          "section": {
            "created_date": "2018-11-10T15:53:00",
            "id": 315,
            "name": "Quantitative Aptitude",
            "test_id": 180,
            "total_time": 600
          },
          "section_id": 315,
          "test_attempt_id": 9564,
          "time_spent": 240,
          "total_question_count": 10
        }
      ],
      "test_id": 180,
      "user_id": 137
    }
    ```
    
    



### Admin → Reset a Test :red_circle:

Reset a Test for an User. This is usually done when a user has faced 'technical' difficulties during the Test.
It should reset test attempt record, reset section attempt time and delete all Question Attempt rows.

!!! custom_req " `POST /admin/tests/attempts/:testAttemptId/reset`"

	`testAttemptId` → The ID of the Test Attempt to Reset


### Admin  → Get Test Attempts :red_circle:

Reporting endpoint for Admins to get Test Attempts of a Test for a particular User or a category of Users.
This helps admin monitor examinations & performance during Beatest Challenges.

!!! get_req " `GET /admin/test_attempts/:test_id/users`"
    ##### Supported Query Params

    | Key        	  | Description                	 |
    |------------------|---------------------------------|
    | test_id     	 | ID for Beatest Exam        	 |
    | email      	  | Email Address for the User 	 |
    | college_id 	  | College ID for Users       	 |

    ##### Response
    ```JSON
    [
        {
            "id": 1,
            "user_id": 12,
            "test_id": 10,
            "is_complete": true,
            "score": 24
        },
        {
            "id": 2,
            "user_id": 12,
            "test_id": 12,
            "is_complete": false,
            "score": 0
        }
    ]
    ```
