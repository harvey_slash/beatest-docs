# File Uploads

In order to support file uploads, we use AWS S3. All files uploaded by 
users are stored S3. 

To prevent conflicts from system libraries, we name our files 'blobs'.
These two terms may be used interchangebly.


## File Database Table 


| Field Name   | Field Type                   | Description                                                                                      |
|--------------|------------------------------|--------------------------------------------------------------------------------------------------|
| id           | `int`                        | unique identifier for the file                                                                   |
| user_id      | `int FK to id in User Table` | the id of the User that uploaded the file<br> (`null` indicates file was uploaded by the system) |
| s3_link      | `varchar (512)`              | the full s3 url for the file.                                                                    |
| mime_type    | `varchar (255)`              | MIME type of the file (E.g. `application/pdf`)                                                   |
| created_date | `datetime`                   | time when the file was created                                                                   |
| updated_date | `datetime                    | time when the file was updated                                                                   |


**Notes**

The s3 link should not be returned to users directly. A signed url using cloudfront should be created and that url should be sent.
Sending the s3 link directly will always give a permission denied error at the client.

## File Upload Process

The client uploads the files as a `Multipart request to Beatest. 
The Beatest server verifies the file based on its mime type and if all validation 
succeeds, it then uploads the file to aws s3. 

When this file is required in the future, only the s3 link is sent to the client. 
The client needs to fetch the file from s3 directly. 

**Note** This s3 link is a signed url with a timeout value. After some duration, 
the file will not be accessible anymore.

```diagram
sequenceDiagram
participant client
participant Beatest
participant Database
participant AWS

client->>Beatest:   Upload file along with other data
Beatest->>Database: Calculate amount & Create New Order
Beatest->>AWS:      Upload file to s3
Beatest->>Database: Add row in Files Table with s3 link 
Beatest-->>client:  return response
```

## Design Guidelines

We exclusively use JSON to communicate between client and server.

To make things consistent yet atomic , all requests that involve 
files will be sent as `Multipart`.

The various parts of the `Multipart` request are given below.

1. There will always be a `body` key.
<br>Regardless whether additional metadata needs to be sent or not, there has to be 
a part containing the `body` key. This key will contain `JSON` in string form and
may contain various metadata about the file.

2. There will always be a `file` key.
The value of this key will contain the file itself. 

3. Under the `body` from 1. there will be one key with the value of `file`.<br>
Note that this is the literal value 'file' and not the data of the file from 2.
This reference will tell the server that that particular key references the file from 2.

4. Additional files can also be present as additional parts. <br>
They need to be in the form `file_1`,`file_2`... `file_N`. For each file , there
must be one key which refers to that file.  


### Examples


A single file upload.


!!! post_req "`POST /some_random_endpoint/thatdoesntexist`"
    ##### Request
    
    Part 1
    
    **Note** The value of body is actually a string.
    ```JSON
    body = '{   "some_key" : 23 ,
                "some_other_kwy": "string_value" ,
                "image": "file" 
            }'

    ```
    
    Part 2
    
    **Note** 'file' is referred to this file in part 1
    ```JSON
    file = <the file from the client>

    ```


Endpoints that have multiple uploads

!!! post_req "`POST /some_random_endpoint/thatdoesntexist`"
    ##### Request
    
    Part 1
    
    **Note** The value of body is actually a string.
    ```JSON
    body = '{   "some_key" : 23 ,
                "some_other_kwy": "string_value" ,
                "image": "file" ,  # first file still named 'file'
                "video": "file_1"  # 2nd file starts with 1
            }'

    ```
    
    Part 2
    
    ```JSON
    file   = <the first file from the client>

    ```


    Part 3
    
    ```JSON
    file_1 = <the 2nd file from the client>

    ```


