## Welcome to Api v0.1

This is the first developer documentation for the endpoints that will be (hopefully) implemented for beatest. 
 
It will also contain other material for developers like pip requirements, best practices, and some standards.


## Documentation Convention


For `GET` requests: 

### Fetch all users
Get all the users from data.

!!! get_req "`GET /test`"
	##### Request Json
	
	```JSON
	{
	    "glossary": {
	        "title": "example glossary",
			"GlossDiv": {
	            "title": "S",
				"GlossList": {
	                "GlossEntry": {
	                    "ID": "SGML",
						"SortAs": "SGML",
						"GlossTerm": "Standard Generalized Markup Language",
						"Acronym": "SGML",
						"Abbrev": "ISO 8879:1986",
						"GlossDef": {
	                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
							"GlossSeeAlso": ["GML", "XML"]
	                    },
						"GlossSee": "markup"
	                }
	            }
	        }
	    }
	}
	
	
	
	```
	##### Response Json
	
	```JSON
	{
	    "glossary": {
	        "title": "example glossary",
			"GlossDiv": {
	            "title": "S",
				"GlossList": {
	                "GlossEntry": {
	                    "ID": "SGML",
						"SortAs": "SGML",
						"GlossTerm": "Standard Generalized Markup Language",
						"Acronym": "SGML",
						"Abbrev": "ISO 8879:1986",
						"GlossDef": {
	                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
							"GlossSeeAlso": ["GML", "XML"]
	                    },
						"GlossSee": "markup"
	                }
	            }
	        }
	    }
	}
	
	
	
	```
	
	
	
