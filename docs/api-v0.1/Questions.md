# Questions
Questions are part of [Sections](Sections.md) , which means they are implicitly a part of 
[Tests](Tests.md).
A Question belongs to exactly 1 Section. 


## Database Table
Tests are currently represented in the database like below

| Field Name     | Data Type                                          | Description                                               |
|----------------|----------------------------------------------------|-----------------------------------------------------------|
| id             | `int (PK)`                                         | Unique identifier for Question                            |
| section_id     | `int (FK to id in `[Section](Sections.md)` table)` | The Section that this Question belongs to                 |
| html           | `longtext`                                         | The contents of the question                              |
| points_correct | `float    `                                        | How many points to be awarded for correct answer          |
| points_wrong   | `float    `                                        | How many points to be deducted for incorrect answer       |
| type           | `varchar(50)  `                                    | The type of question (Read below for details)             |
| tita_answer    | `longtext `                                        | text for 'Type In The Answer' style questions             |
| rc_passage     | `longtext `                                        | Passage for Reading Comprehension style questions         |
| logic          | `longtext     `                                    | The html that stores the process of solving this question |
| lod            | `varchar(50)    `                                  | Level of Difficulty                                       |
| topic          | `varchar(50)    `                                  | The topic of the question                                 |





#### Question types
A Question can be of 3 types: _MCQ_, _RC_ , or _TITA_. Each Type is described below

| Type | Description                                                                                                                                 |
|------|---------------------------------------------------------------------------------------------------------------------------------------------|
| MCQ  | Stands for Multiple Choice Question. Can have one of several choices. All the choices will be displayed to the user,can select one of them. |
| RC   | Stands for Reading Comprehension. **DEFINE**                                                                                                |
| TITA | Stands for Type In The Answer. Meant for answers with a numerical response                                                                  |

Depending on the Question type, some fields in the row will be `NULL`. If the Question type is _MCQ_, then `tita_answer`  as well as `rc_passage` will be `NULL`. 
If the Question type is _RC_ , then `tita_answer` will be `NULL`. If the type is _tita_answer_, then `rc_passage` will be `NULL`.


## API Reference

<!-- ### List Question IDs for Section :red_circle: -->

<!-- Get Question IDs for a Section. -->


<!-- This endpoint will only return information if there exists a Test Attempt -->
<!-- by the user that is active. In the case of Sections that have limited time,  -->
<!--    this section will return information only if the Section is open. -->

<!-- !!! get_req "```GET /tests/sections/:sectionID/questions```" -->
<!-- 	`sectionID` → the ID for the section to list questions of -->


<!-- 	##### Response [1] -->
<!-- 	```JSON -->
<!-- 	[ -->
<!-- 		{ -->
<!-- 			"id": 12,   # question id  -->
<!-- 		}, -->
<!-- 		{ -->
<!-- 			"id": 13,   # question id  -->
<!-- 		} -->
<!-- 	] -->
<!-- 	``` -->
<!-- 	##### Response [2] -->
<!-- 	if there exists no test attempt that is active, or if the Section is  -->
<!--        	closed	this endpoint -->
<!-- 	fails with `HTTP Code 403`	 -->

### Get Question Details :white_check_mark:

Get details of a single question. This endpoint returns information only 

**Note** This endpoint sends `Cache-Control` Headers, which means it will be
cached by browsers.

!!! get_req "```GET /tests/:testID/sections/:sectionID/questions/<questionID>```"

	`testID` →  the ID of the test that owns the Sections

	`sectionID` →  the ID of the section that owns the Question

	`questionID` → the ID for the question. 
    
    If the question is **MCQ** type, the choices will be returned as well
    (otherwise it will be an empty array)

	##### Response [1]
	A Question object is returned along with the choices (if any).
	```JSON
	{
		"id": 12,               # question id 
		"section_id" : 31,      # id of section (will be equal to the :sectionID param in url)
		"html": "<long_html>"  
        "choices":  # empty array if the question is not mcq or RC
                  [
                        {
                        "html":"<long_html>",
                        "id":1871
                        },
                        {
                        "html":"<long_html>",
                        "id":1874
                        },
                        {
                        "html":"<long_html>",
                        "id":1878
                        },
                  ]
    "rc_passage" : "<long_html>" # null if its not an RC type question
        
	}
	```

	##### Response [2]
	if there exists no test attempt that is active, this endpoint
	fails with `HTTP Code 403`	



### Get Question Solutions :white_check_mark:

Get solutions of a single question.
This endpoint returns information only if the test is marked complete.

**Note** This endpoint sends `Cache-Control` Headers, which means it will be
cached by browsers.

!!! get_req "```GET /tests/:testID/sections/:sectionID/questions/<questionID>```"

	`testID` →  the ID of the test that owns the Sections

	`sectionID` →  the ID of the section that owns the Question

	`questionID` → the ID for the question. 
    
    If the question is **MCQ** type, the choices will be returned as well
    (otherwise it will be an empty array)

	##### Response [1]
	A Question object is returned along with the choices (if any).
	```JSON
	{
		"id": 12,               # question id 
		"section_id" : 31,      # id of section (will be equal to the :sectionID param in url)
        "tita_answer": 1872,     # correct answer (will be null if not tita type answer)
        "logic": "<long_html>"  #  procedure to solve the question
        "choices": [# will be an array, but should only contain 1 element if MCQ or RC type question. 
                    #will be empty if TITA type
        
        
                        {
                        "id":1871 # choice id
                        "is_correct": true # if the choice is the correct one.
                        },
                  ]

        
	}
	```

	##### Response [2]
	if there exists no test attempt that is active, this endpoint
	fails with `HTTP Code 403`	

### Admin → Add a Question 

Add a Question to the system. Only to be performed by an admin. 

This requires the associated Section to already have been created.  

 
 
!!! post_req "```POST /admin/tests/sections/:sectionID/questions```"
	`sectionID` the id for the test to add this question under
	 
	##### Request
	The question type constraints mentioned above apply here. Depending on question type field, the system will just
	 look for the associated key, and ignore other types (E.g. if type is RC, then only the `rc_passage` key will be selected.
	 
	```JSON
	{
		"section_id": 213,                     
		"html": "long_html",
		"points_correct": 1,
		"points_wrong": .25,
		"type": "RC",
		"rc_passage":"<long_html>",
		"logic": " **DONTKNOW** ",  **DONT KNOW!!!!!!!!!!!!!!!**
		"lod":"easy",
		"topic":"science"
		
	}
	```
	##### Response
	```JSON
	{
		"id": 12    # the newly created id for the question
	}
	```


### Admin → Update a Section

Update a Section to the system. Only to be performed by an admin.



!!! put_req "```PUT /admin/tests/sections/questions/:questionID```"
	`questionID` -- The id of the question to modify
	
	##### Request
	The question type constraints mentioned above apply here. Depending on question type field, the system will just
	 look for the associated key, and ignore other types (E.g. if type is RC, then only the `rc_passage` key will be selected.
	 
	```JSON
	{
		"section_id": 213,                     
		"html": "long_html",
		"points_correct": 1,
		"points_wrong": .25,
		"type": "RC",
		"rc_passage":"<long_html>",
		"logic": " **DONTKNOW** ",  **DONT KNOW!!!!!!!!!!!!!!!**
		"lod":"easy",
		"topic":"science"
		
	}
	```
	**Note** Remember to set rc_passage and tita_answer to `NULL` , as an update on question type may have both of them to be `NOT NULL`
    

### Admin → Delete a Question

Delete a Question in the system. Only to be performed by an admin.

It is very unlikely that this this endpoint will be called. 


!!! del_req "```DELETE /admin/tests/sections/questions/:questionID```"
     
