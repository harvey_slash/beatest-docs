# Running Code APIs


### Run Code Against tests (UNSTABLE)


Run a code snippet against a list of provided tests.


!!! post_req " ```POST /api/snippet/run```"


    ##### Request

    This endpoint requires 


	```JSON
	{
        "lang":"python3.6"   # must be one of supported languages
		"snippet": "for i in range(10): print(i)",     # code snippet. Must NOT Be minified.
		"test_cases": [      # list of test cases, must NOT be minified
              
              "1,2,3,4,"
              "8,1,2,41"
        ]
	}
	```
    
    #### Response
    After running the snippet against the test cases, the following response is returned.
    
    It is an array containing the output of each test case , in the same order as the test cases.
	```JSON
	[
              "1,2,3,4,"
              "8,1,2,4"
	]
	```

