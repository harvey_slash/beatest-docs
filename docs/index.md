# Beatest Documentation

This is a developer oriented resource to understand and document the core Beatest system


## Rendering the the docs 


This uses MkDocs https://github.com/squidfunk/mkdocs-material


Before rendering , install the following dependencies using `pip` like so

```
pip install mkdocs-material
pip install pymdown-extensions

```


Then you can render by going to the `docs` directory in the source repo, and run 
```

mkdocs serve

```

This should run a local server on port `8000` , which you can access by visiting `http://localhost:8000`

This is the most updated documentation. 

