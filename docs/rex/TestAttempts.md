
# TestAttempts

A rex admin can view the results of test attempts as long he has the right permissions. 
The permissions are : 

1. Rex admin has access to monitor the test
2. Rex admin has access to monitor the user



### Get Overview :white_check_mark: :computer:

Get overview data of a list of test attempts. 



!!! get_req "`GET /test/<test_id>/attempts`"

    ##### Response

    ```
    {
      "allow_section_jumps": false, # These are details about the test itself
      "character": "Mock",
      "created_date": "2018-09-08T11:07:00",
      "id": 170,
      "is_active": true,
      "leaderboard_id": null,
      "logo": null,
      "name": "Beat Placement CTS #1",
      "price": 0,
      "sections": [
        {
          "created_date": "2018-09-08T11:08:00",
          "id": 290,
          "name": "Logical Reasoning",
          "test_id": 170,
          "total_time": 840
        },
        {
          "created_date": "2018-09-08T11:09:00",
          "id": 291,
          "name": "Quantiative Aptiitude",
          "test_id": 170,
          "total_time": 960
        },
        {
          "created_date": "2018-09-08T11:09:00",
          "id": 292,
          "name": "Verbal Ability",
          "test_id": 170,
          "total_time": 1500
        },
        {
          "created_date": "2018-09-08T11:09:00",
          "id": 293,
          "name": "Automata Fix",
          "test_id": 170,
          "total_time": 1200
        }
      ],
      "test_attempts": [ # only those users who are applicants to the corporate will be displayed
        {
          "date": "2019-01-17T06:28:23",
          "id": 9569,
          "is_complete": true,
          "is_graded": true,
          "score": 6,
          "section_attempts": [
            {
              "correct_question_count": 6,
              "id": 23035,
              "incorrect_question_count": 0,
              "is_complete": false,
              "score": 6,
              "section_id": 290,
              "test_attempt_id": 9569,
              "time_spent": 350
            },
            {
              "correct_question_count": 0,
              "id": 23036,
              "incorrect_question_count": 0,
              "is_complete": false,
              "score": 0,
              "section_id": 291,
              "test_attempt_id": 9569,
              "time_spent": 0
            },
            {
              "correct_question_count": 0,
              "id": 23037,
              "incorrect_question_count": 0,
              "is_complete": false,
              "score": 0,
              "section_id": 292,
              "test_attempt_id": 9569,
              "time_spent": 0
            },
            {
              "correct_question_count": 0,
              "id": 23038,
              "incorrect_question_count": 0,
              "is_complete": false,
              "score": 0,
              "section_id": 293,
              "test_attempt_id": 9569,
              "time_spent": 0
            }
          ],
          "test_id": 170,
          "user": { # the user who's test attempt this is.
            "application": { # the user's application status 
              "corporate_id": 1,
              "date": null,
              "type": "accepted", # his status for this corporate
              "user_id": 8847
            },
            "college": {
              "college_name": "Greater Kolkata College of Engg and Management", # name of user's college
              "id": 109
            },
            "full_name": "PRASUN MONDAL",
            "id": 8847
          },
          "user_id": 8847
        }
      ],
      "type": "CAT"
    }
    ```

