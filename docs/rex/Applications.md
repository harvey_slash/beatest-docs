# Applications

An application is a connection between a user and a corporate.




### Update Applications :white_check_mark: :computer:

Update application `type` for a list of users.


!!! put_req "`GET /applications`"

    ##### Request

    ```
    "user_ids": [ # list of user ids to update the applications of
    1,
    2,
    4,
    5
    ],
    "status": "accepted" # must be one of 'accepted,rejected,shortlisted,null'
    
    ```
